﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4_Task
{
    public class Car : IDrivable
    {
        public void Start()
        {
            Console.WriteLine("Car is started...");
        }
        public void Drive()
        {
            Console.WriteLine("car Driving Started...");
        }        
        public void Stop()
        {
            Console.WriteLine("Car is stopped");
        }
    }

    public class BiCycle:IDrivable
    {
        public void Start()
        {
            Console.WriteLine("BiCycle is started...");
        }
        public void Drive()
        {
            Console.WriteLine("BiCycle Driving Started...");
        }
        public void Stop()
        {
            Console.WriteLine("BiCycle is stopped");
        }
    }

    class task2_IDrivable
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            car.Start();
            car.Drive();
            car.Stop();
            BiCycle biCycle = new BiCycle();
            biCycle.Start();
            biCycle.Drive();
            biCycle.Stop();
            Console.ReadLine();
        }
    }
}
