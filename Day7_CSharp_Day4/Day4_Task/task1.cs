﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4_Task
{

    /*
         1)	Design a base class Employee with properties for name, title, 
        and base salary. Create a derived class Manager that inherits 
        from Employee and includes a method to calculate a bonus based 
        on performance. Override the method to display employee details 
        to include bonus information for managers.
     */
    public class Employee
    {
        public string name;
        public string title;
        public double baseSalary;
        public double bonus;
       public  void getDetails()
        {
            Console.WriteLine("Enter name:");
            name = Console.ReadLine();
            Console.WriteLine("Enter Title:");
            title = Console.ReadLine();
            Console.WriteLine("Enter your Base Salary:");
            baseSalary=Convert.ToDouble(Console.ReadLine());
        }

      public   void displayMainDetails()
        {
            Console.WriteLine("YOur Name:" + name);
            Console.WriteLine("Your Title:" + title);
            Console.WriteLine("YOur Basic Salary:" + baseSalary);
        }

    }
    class Manager : Employee
    {
       public  void CalculateBonus()
        {
            Console.WriteLine("How is you performance:\t press 1.Excellent 2.better 3.good 4.bad==>");
            int ch = Convert.ToInt32(Console.ReadLine());
        
            if(ch==1)            
                bonus = 0.5;
            if (ch == 2)
                bonus = 0.3;
            if (ch == 3)
                bonus = 0.2;
            if (ch == 4)
                bonus = 0.1;
            Console.WriteLine("Your Bonus is:" + (baseSalary * bonus));
        }
    }


    class task1
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            manager.getDetails();
            manager.displayMainDetails();
            manager.CalculateBonus();
            Console.ReadLine();
            
        }
    }
}
