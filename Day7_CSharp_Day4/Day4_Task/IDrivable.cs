﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4_Task
{
    /*
     1)	Define an interface IDrivable with methods for Start, Stop, and Drive.
        Implement this interface in two classes, Car and Bicycle. 
        Demonstrate polymorphism by writing a method that takes 
        an IDrivable object and invokes each method.
     */
    interface IDrivable
    {
        void Start();
        void Stop();
        void Drive();
    }
}
